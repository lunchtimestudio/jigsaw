package com.lunchtimestudio.jigsaw;

/**
 * Created by mfoenko on 1/7/16.
 */
public class Constants {

    //locks up constructor;
    private Constants(){

    }

    public static final int UP = 0;
    public static final int RIGHT = 1;
    public static final int DOWN = 2;
    public static final int LEFT = 3;
    public static final int NUM_DIRECTIONS = 4;

    public static final float RANGE = 30;

}
