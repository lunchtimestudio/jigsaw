package com.lunchtimestudio.jigsaw;

import com.badlogic.gdx.Game;
import com.lunchtimestudio.jigsaw.screen.GameScreen;

public class JigsawGame extends Game {

	@Override
	public void create() {
		setScreen(new GameScreen());
	}
}
