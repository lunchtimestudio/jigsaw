package com.lunchtimestudio.jigsaw.puzzle;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;
import com.lunchtimestudio.jigsaw.Constants;

/**
 * Created by mfoenko on 1/7/16.
 */
public class PuzzlePiece {


    public final TextureRegion image;
    public final int width;
    public final int height;
    public final PuzzlePiece[] connectingPieces = new PuzzlePiece[Constants.NUM_DIRECTIONS];

    public PuzzlePiece(TextureRegion image, int w, int h,  PuzzlePiece... connectingPieces){
        this(image, w, h);
        for(int i=0;i<this.connectingPieces.length;i++) {
            this.connectingPieces[i] = connectingPieces[i];
        }
    }

    public PuzzlePiece(TextureRegion image, int w, int h) {
        this.image = image;
        this.width = w;
        this.height = h;
    }

    public PuzzlePiece getConnectingPiece(int direction){
        //return getConnectingPiece(direction, 0);
        return connectingPieces[direction];
    }

    public PuzzlePiece getConnectingPiece(int direction, int rotation){
        return getConnectingPiece((direction-rotation+Constants.NUM_DIRECTIONS)%Constants.NUM_DIRECTIONS);
    }

}
