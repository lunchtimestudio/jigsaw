package com.lunchtimestudio.jigsaw.puzzle;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.lunchtimestudio.jigsaw.Constants;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by mfoenko on 1/7/16.
 */
public class PuzzleCluster {

    public static final int PIECE_WIDTH = 50;
    public static final int PIECE_HEIGHT = 50;

    public final ArrayList<PuzzleClusterNode> nodes = new ArrayList<PuzzleClusterNode>();

    public float x = 0;
    public float y = 0;
    public int rotation = 0;

    public PuzzleCluster(PuzzleClusterNode startingNode) {
        nodes.add(startingNode);
        calculateNodePositions();
    }

    public boolean canConnect(PuzzleCluster other) {
        if (rotation != other.rotation) {
            return false;
        }

        for (PuzzleClusterNode node: nodes) {
            for (int d = 0; d < Constants.NUM_DIRECTIONS; d++) {
                if (node.connectedNodes[d] == null && node.piece.connectingPieces[d] != null) {
                    for (PuzzleClusterNode otherNode : other.nodes){
                        if(otherNode.piece == node.piece.connectingPieces[d] && isInRange(node, otherNode, d)){
                            return true;
                        }
                    }
                }
            }


        }

        return false;
    }



    public boolean isInRange(PuzzleClusterNode a, PuzzleClusterNode b, int direction){
        switch (direction){
            case Constants.UP:
                return Math.abs(b.x-a.x)<= Constants.RANGE && Math.abs(b.y+b.getHeight()-a.y) <= Constants.RANGE;
            case Constants.DOWN:
                return Math.abs(b.x-a.x)<= Constants.RANGE && Math.abs(a.y+a.getHeight()-b.y) <= Constants.RANGE;
            case Constants.LEFT:
                return Math.abs(b.x+b.getWidth()-a.x)<= Constants.RANGE && Math.abs(b.y-a.y) <= Constants.RANGE;
            case Constants.RIGHT:
                return Math.abs(a.x+a.getWidth()-b.x)<= Constants.RANGE && Math.abs(b.y-a.y) <= Constants.RANGE;
        }
        return false;
    }

    public boolean add(PuzzleCluster cluster) {
        if (nodes.size() == 0) {
            nodes.addAll(cluster.nodes);
            return true;
        }

        boolean match = false;
        for (PuzzleClusterNode otherNode : cluster.nodes) {
            for (PuzzleClusterNode thisNode : nodes) {
                for (int d = 0; d < Constants.NUM_DIRECTIONS; d++) {
                    if (otherNode.piece.getConnectingPiece(d, rotation) == thisNode.piece) {
                        match = true;
                        otherNode.setConnectedNode(thisNode, d, rotation);
                        thisNode.setConnectedNode(otherNode, (d + 2) % Constants.NUM_DIRECTIONS, rotation);
                    }
                }
            }
        }
        if (match) {
            nodes.addAll(cluster.nodes);
            calculateNodePositions();
        }

        return match;
    }

    public void calculateNodePositions() {
        if (nodes.size() > 0)
            recursiveCalculateNodePositions(nodes.get(0), new ArrayList<PuzzleClusterNode>(), new int[Constants.NUM_DIRECTIONS]);
    }

    //TODO replace ArrayList of drawn object with binary-searchable list
    private void recursiveCalculateNodePositions(PuzzleClusterNode node, ArrayList<PuzzleClusterNode> alreadyCalced, int[] path) {
        if (alreadyCalced.contains(node) || node == null) {
            return;
        }


        float x = this.x
                + path[Constants.RIGHT] * node.getWidth()
                - path[Constants.LEFT] * node.getWidth();
        float y = this.y
                - path[Constants.UP] * node.getHeight()
                + path[Constants.DOWN] * node.getHeight();

        node.x = x;
        node.y = y;


        alreadyCalced.add(node);


        for (int i = 0; i < Constants.NUM_DIRECTIONS; i++) {
            if (node.connectedNodes[i] != null) {
                int[] newPath = Arrays.copyOf(path, Constants.NUM_DIRECTIONS);
                newPath[i]++;
                recursiveCalculateNodePositions(node.connectedNodes[i], alreadyCalced, newPath);
            }
        }
    }

    public void draw(Batch batch) {
        for (PuzzleClusterNode node : nodes) {
            node.draw(batch);
        }
    }

    public boolean isTouched(float x, float y) {
        for (PuzzleClusterNode node : nodes) {
            if (x > node.x && x < node.x + node.piece.width &&
                    y > node.y && y < node.y + node.piece.height) {
                return true;
            }
        }
        return false;
    }


}
