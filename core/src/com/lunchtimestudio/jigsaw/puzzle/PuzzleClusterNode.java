package com.lunchtimestudio.jigsaw.puzzle;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.lunchtimestudio.jigsaw.Constants;

/**
 * Created by mfoenko on 1/7/16.
 */
public class PuzzleClusterNode {

    public final PuzzlePiece piece;
    public final PuzzleClusterNode[] connectedNodes = new PuzzleClusterNode[Constants.NUM_DIRECTIONS];

    public float x;
    public float y;
    public float rotation;

    public PuzzleClusterNode(PuzzlePiece piece) {
        this.piece = piece;
    }

    public void setConnectedNode(PuzzleClusterNode node, int direction, int offset){
        this.connectedNodes[(direction-offset+ Constants.NUM_DIRECTIONS)%Constants.NUM_DIRECTIONS] = node;
    }

    public int getWidth(){
        return piece.width;
    }

    public int getHeight(){
        return piece.height;
    }

    public void draw(Batch batch){
        batch.draw(piece.image,
                x, y,
                piece.width / 2, piece.height / 2,
                piece.width, piece.height,
                1f, 1f,
                rotation * 90
        );
    }
}
