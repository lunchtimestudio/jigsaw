package com.lunchtimestudio.jigsaw.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.lunchtimestudio.jigsaw.Constants;
import com.lunchtimestudio.jigsaw.PuzzleInputProcessor;
import com.lunchtimestudio.jigsaw.puzzle.PuzzleCluster;
import com.lunchtimestudio.jigsaw.puzzle.PuzzleClusterNode;
import com.lunchtimestudio.jigsaw.puzzle.PuzzlePiece;

import java.util.ArrayList;

/**
 * Created by mfoenko on 1/7/16.
 */
public class GameScreen implements Screen{

    Batch batch;
    OrthographicCamera camera;

    Texture image;
    public PuzzlePiece[][] pieces;
    public ArrayList<PuzzleCluster> clusters;

    @Override
    public void show() {

        batch = new SpriteBatch();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false);
        camera.position.set(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2,0);
        clusters = new ArrayList<PuzzleCluster>();

        //TODO make these variables come in from constructor
        image = new Texture(Gdx.files.internal("eifel.jpg"));
        int numPiecesWide = 8;
        int numPiecesHigh = 6;

        pieces = new PuzzlePiece[numPiecesWide][numPiecesHigh];

        //TODO figure out something for not evenly dividing numbers
        //TODO make edge pieces bigger?
        int pieceWidth = image.getWidth() / numPiecesWide;
        int pieceHeight = image.getHeight() / numPiecesHigh;
        int imageWidth = image.getWidth() / numPiecesWide;
        int imageHeight  = image.getHeight() / numPiecesHigh;

        //split up image into pieces
        for (int x = 0; x < numPiecesWide; x++) {
            for (int y = 0; y < numPiecesHigh; y++) {
                //TODO draw puzzle joints
                Pixmap px = new Pixmap()


                TextureRegion region = new TextureRegion(image, x * imageWidth, y * imageHeight, imageWidth, imageHeight);
                pieces[x][y] = new PuzzlePiece(region, pieceWidth, pieceHeight);
            }
        }


        //tell each piece what pieces it's looking for
        for (int x = 0; x < numPiecesWide; x++) {
            for (int y = 0; y < numPiecesHigh; y++) {
                for (int d = 0; d < Constants.NUM_DIRECTIONS; d++) {
                    PuzzlePiece p = null;
                    switch (d) {
                        case Constants.DOWN:
                            if (y > 0) {
                                p = pieces[x][y - 1];
                            }
                            break;
                        case Constants.UP:
                            if (y < numPiecesHigh - 1) {
                                p = pieces[x][y + 1];
                            }
                            break;
                        case Constants.LEFT:
                            if (x > 0) {
                                p = pieces[x - 1][y];
                            }
                            break;

                        case Constants.RIGHT:
                            if (x < numPiecesWide - 1) {
                                p = pieces[x + 1][y];
                            }
                            break;
                    }
                    pieces[x][y].connectingPieces[d] = p;

                }
            }
        }

        //TODO make starting clusters blank and let user drag pieces onto board
        for (int x = 0; x < numPiecesWide; x++) {
            for (int y = 0; y < numPiecesHigh; y++) {
                PuzzleCluster cluster = new PuzzleCluster(new PuzzleClusterNode(pieces[x][y]));
                clusters.add(cluster);

                cluster.x = (int)(Math.random() * Gdx.graphics.getWidth());
                cluster.y = (int)(Math.random() * Gdx.graphics.getHeight());
                cluster.rotation = 0;//(int)(Math.random() * Constants.NUM_DIRECTIONS);
                cluster.calculateNodePositions();

            }
        }

        Gdx.input.setInputProcessor(new PuzzleInputProcessor(clusters, camera));
    }

    @Override
    public void render(float delta) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        for(PuzzleCluster cluster: clusters){
            cluster.draw(batch);
        }
        batch.end();


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        image.dispose();
    }

    @Override
    public void resume() {

    }
}
