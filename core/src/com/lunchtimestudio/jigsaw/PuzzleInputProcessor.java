package com.lunchtimestudio.jigsaw;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;
import com.lunchtimestudio.jigsaw.puzzle.PuzzleCluster;

import java.util.ArrayList;

/**
 * Created by mfoenko on 1/8/16.
 */
public class PuzzleInputProcessor implements InputProcessor {

    Camera camera;
    ArrayList<PuzzleCluster> clusters;
    int selectedIndex = -1;
    float xOffset;
    float yOffset;

    public PuzzleInputProcessor(ArrayList<PuzzleCluster> clusters, Camera camera) {
        this.clusters = clusters;
        this.camera = camera;
    }


    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector3 vect = camera.unproject(new Vector3(screenX, screenY, 0));

        for(int i=clusters.size() -1;i>=0;i--){
            if(clusters.get(i).isTouched(vect.x,vect.y)){
                clusters.add(clusters.remove(i));
                selectedIndex = clusters.size()-1;
                xOffset = vect.x-clusters.get(selectedIndex).x;
                yOffset = vect.y-clusters.get(selectedIndex).y;
                break;
            }
        }
        return selectedIndex != -1;
    }



    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(selectedIndex != -1) {

            boolean connect;
            PuzzleCluster clust = clusters.get(selectedIndex);
            do {
                connect = false;
                for (int i = 1; i < clusters.size(); i++) {
                    if (i == selectedIndex) {
                        continue;
                    }
                    if (clust.canConnect(clusters.get(i))) {
                        clust.add(clusters.get(i));
                        clusters.remove(i);
                        connect = true;
                        break;
                    }

                }
            } while (connect);
        }
        selectedIndex = -1;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        Vector3 vect = camera.unproject(new Vector3(screenX, screenY, 0));

        if(selectedIndex == -1) return false;
        PuzzleCluster cluster = clusters.get(selectedIndex);
        cluster.x = vect.x-xOffset;
        cluster.y = vect.y-yOffset;
        cluster.calculateNodePositions();
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
